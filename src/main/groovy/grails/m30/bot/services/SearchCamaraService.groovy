package grails.m30.bot.services

import grails.m30.bot.AppConfig
import grails.m30.bot.Camara
import grails.m30.bot.SearchCamaraResult
import groovy.transform.Synchronized
import groovy.util.logging.Log

@Log
abstract class SearchCamaraService {

    AppConfig appConfig

    List<Camara> camaras=[]

    abstract void init(AppConfig appConfig)

    @Synchronized
    List<Camara> camaras(){
        if( camaras.size() == 0)
            init(appConfig)
        camaras
    }

    SearchCamaraResult searchCamara(final String search){
        SearchCamaraResult ret = new SearchCamaraResult(search: search)
        try {
            final String searchUpperCase = search.toUpperCase()
            boolean byNumber = searchUpperCase.isNumber()
            def places = camaras().findAll { camara ->
                if (byNumber)
                    return camara.id == (searchUpperCase as int)
                else
                    return camara.nombre.indexOf(searchUpperCase) != -1
            }

            if (places.size()) {
                def first = places.first()
                ret.name = first.toString()
                ret.image = first.url.toURL().bytes
                ret.alternates = places.drop(1)
            }
            log.info "Search $search found ${ret.alternates} places"
        }catch(e){
            log.severe e.toString()
        }
        ret
    }
}
