package grails.m30.bot.services

import com.puravida.gif.GifGenerator
import grails.m30.bot.AppConfig
import groovy.transform.CompileStatic
import groovy.transform.Synchronized
import groovy.transform.TypeCheckingMode
import groovy.util.logging.Log
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value

import java.util.concurrent.TimeUnit
@CompileStatic(TypeCheckingMode.SKIP)
@Log
class M30Service {

    NodeList Camara

    M30Service(AppConfig appConfig){
        root = File.createTempDir()
        gifImage = new File(root.parentFile, "m30.gif")
        Camara = new XmlParser().parse(appConfig.m30Url).Camara
    }

    String currentStatus(){

        def DatosTrafico = new XmlSlurper().parse("https://www.mc30.es/images/xml/DatosTrafico.xml")
        def datosGlobales = DatosTrafico.DatoGlobal
        def totalVehiculosTunel = datosGlobales.find { it.Nombre.text() == 'totalVehiculosTunel' }.VALOR.text()
        def totalVehiculosCalle30 = datosGlobales.find { it.Nombre.text() == 'totalVehiculosCalle30' }.VALOR.text()
        def velocidadMediaTunel = datosGlobales.find { it.Nombre.text() == 'velocidadMediaTunel' }.VALOR.text()
        def velicidadMediaSuperfice = datosGlobales.find {
            it.Nombre.text() == 'velicidadMediaSuperfice'
        }.VALOR.text()

        String currentStatus = """Situación actual de vehículos/velocidad:
Velocidad Media Tunel: ${velocidadMediaTunel} km/h
Velocidad Media Superficie: ${velicidadMediaSuperfice}  km/h
Total Vehiculos M30: ${totalVehiculosCalle30}
"""
        log.info currentStatus
        currentStatus
    }

    File root
    File gifImage

    @Synchronized
    File getGif() {
        def difference = new Date().time - gifImage.lastModified()
        if (difference < TimeUnit.MINUTES.toMillis(10)) {
            return gifImage
        }

        renewGif()
    }

    File renewGif(){
        List fotos = Camara.findAll {
            "${it.Nombre.text()}".startsWith("M30-PK")
        }.sort{
            "${it.Nombre.text()}"
        }

        List<byte[]>bytes = []
        fotos.eachWithIndex { item, idx ->
            byte[] img = ("http://" + item.URL.text()).toURL().bytes
            bytes.add img
        }

        if( bytes )
            gifImage.bytes = new GifGenerator().composeBytes(bytes, 2000, true)

        gifImage
    }

}
