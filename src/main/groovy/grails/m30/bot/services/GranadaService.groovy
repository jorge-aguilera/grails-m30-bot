package grails.m30.bot.services

import grails.m30.bot.AppConfig
import grails.m30.bot.Camara
import groovy.transform.CompileStatic
import groovy.transform.TypeCheckingMode
import groovy.util.logging.Log
import org.cyberneko.html.parsers.SAXParser
@CompileStatic(TypeCheckingMode.SKIP)
@Log
class GranadaService extends SearchCamaraService{

    void init(AppConfig appConfig) {
        SAXParser parser = new SAXParser()
        parser.setFeature('http://xml.org/sax/features/namespaces',false)

        def html = new XmlSlurper(parser).parseText( appConfig.granadaUrl.toURL().text )
        html."**".findAll { it."@class"=='alaizquierda camara'}.eachWithIndex { item, idx ->

            Camara c = new Camara(ciudad: 'granada',
                    id: idx,
                    nombre: item.P.IMG."@alt".text(),
                    url:  'http://www.movilidadgranada.com'+item.P.IMG."@src".text()
            )
            camaras.add c
        }
    }
}
