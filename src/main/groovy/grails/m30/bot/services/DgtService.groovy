package grails.m30.bot.services

import com.puravida.gif.GifGenerator
import grails.m30.bot.AppConfig
import groovy.transform.CompileStatic
import groovy.transform.Synchronized
import groovy.transform.TypeCheckingMode
import groovy.util.logging.Log
import java.util.concurrent.TimeUnit
@CompileStatic(TypeCheckingMode.SKIP)
@Log
class DgtService {

    File root

    Map<String, File> files = [:]

    AppConfig appConfig

    void init(){
        root = File.createTempDir()
        files['m40norte'] = new File(root.parentFile, "m40Norte.gif")
        files['m40sur'] = new File(root.parentFile, "m40Sur.gif")
        files['m40este'] = new File(root.parentFile, "m40Este.gif")
        files['m40oeste'] = new File(root.parentFile, "m40Oeste.gif")
        files['a6'] = new File(root.parentFile, "a6.gif")
        files['m111'] = new File(root.parentFile, "m111.gif")
        files['a5entrada'] = new File(root.parentFile, "a5entrada.gif")
        files['a5salida'] = new File(root.parentFile, "a5salida.gif")
    }

    @Synchronized
    File getGif(String service){
        if( !root )
            init()

        File gifImage = files[service]
        String[] listImages = appConfig."$service"

        def difference = new Date().time - gifImage.lastModified()
        if (difference < TimeUnit.MINUTES.toMillis(10)) {
            return gifImage
        }

        List<byte[]>bytes = []
        listImages.eachWithIndex { item, idx ->
            byte[] img = ("$appConfig.dgtUrl/${item}.jpg").toURL().bytes
            bytes.add img
        }

        if( bytes )
            gifImage.bytes = new GifGenerator().composeBytes(bytes, 2000, true)

        gifImage
    }


}
