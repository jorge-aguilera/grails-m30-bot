package grails.m30.bot.services

import grails.m30.bot.AppConfig
import grails.m30.bot.Camara
import groovy.transform.CompileStatic
import groovy.transform.TypeCheckingMode
import groovy.util.logging.Log
import groovy.util.slurpersupport.GPathResult
import org.springframework.stereotype.Component
@CompileStatic(TypeCheckingMode.SKIP)
@Log
class MadridService extends SearchCamaraService{

    void init(AppConfig appConfig){
        String xml
        GPathResult kml

        //primer byte tiene un caracter desconocido
        xml = "http://informo.munimadrid.es/informo/tmadrid/CCTV.kml".toURL().text.substring(1)
        kml = new XmlSlurper().parseText(xml).declareNamespace("xmlns":"http://earth.google.com/kml/2.2")

        kml.Document.Placemark.sort{
            it.ExtendedData.Data[1].Value.text()
        }.eachWithIndex{ item, idx->
            String description = item.description.text()
            String url = description.split(' ').find{ it.startsWith('src=')}.substring(4)
            Camara c = new Camara( ciudad: 'madrid',
                    id: idx,
                    nombre:item.ExtendedData.Data[1].Value.text(),
                    url:url
            )
            camaras.add c
        }
    }

}
