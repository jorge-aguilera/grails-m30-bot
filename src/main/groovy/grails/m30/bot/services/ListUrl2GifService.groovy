package grails.m30.bot.services

import com.puravida.gif.GifGenerator
import grails.m30.bot.AppConfig
import groovy.transform.CompileStatic
import groovy.transform.Synchronized
import groovy.transform.TypeCheckingMode
import org.springframework.beans.factory.annotation.Autowired

import javax.annotation.PostConstruct
import java.util.concurrent.TimeUnit

abstract class ListUrl2GifService {

    abstract String[] getListUrls()

    abstract String getServiceName()

    AppConfig appConfig

    File root
    File gifImage

    @Synchronized
    File getGif() {
        if( gifImage == null || gifImage.length() == 0)
            return renewGif()
        def difference = new Date().time - gifImage.lastModified()
        if (difference < TimeUnit.MINUTES.toMillis(10)) {
            return gifImage
        }

        renewGif()
    }

    @CompileStatic(TypeCheckingMode.SKIP)
    File renewGif(){
        root = File.createTempDir()
        gifImage = new File(root.parentFile, "${serviceName}.gif")

        List<byte[]>bytes = []
        listUrls.eachWithIndex { item, idx ->

            byte[] img = "$item".toURL().getBytes(
                    connectTimeout: 10*1000, readTimeout: 10*1000,
                    useCaches: true, allowUserInteraction: false,
                    requestProperties: [
                            'Accept': '*/*',
                            'Cache-Control':'max-age=0',
                            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.81 Safari/537.36'
                    ]
            )
            bytes.add img
        }

        if( bytes )
            gifImage.bytes = new GifGenerator().composeBytes(bytes, 2000, true)

        gifImage
    }

}
