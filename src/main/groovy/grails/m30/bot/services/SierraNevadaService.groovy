package grails.m30.bot.services

import groovy.util.logging.Log

@Log
class SierraNevadaService extends ListUrl2GifService{

    @Override
    String getServiceName() {
        "sierraNevada"
    }

    @Override
    String[] getListUrls() {
        appConfig.sierraNevadaCamaras
    }

}
