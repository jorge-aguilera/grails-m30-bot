package grails.m30.bot.services

import grails.m30.bot.AppConfig
import grails.m30.bot.Camara
import groovy.transform.CompileStatic
import groovy.transform.TypeCheckingMode
import groovy.util.logging.Log
import groovy.xml.Namespace
@CompileStatic(TypeCheckingMode.SKIP)
@Log
class BarcelonaService extends SearchCamaraService{

    Namespace gml = new Namespace("http://www.opengis.net/gml", 'gml')
    Namespace cite = new Namespace("http://www.opengeospatial.net/cite", 'cite')

    Node kml

    void init(AppConfig appConfig) {
        String xml = appConfig.barcelonaUrl.toURL().text
        kml = new XmlParser().parseText(xml)

        kml[gml.featureMember].eachWithIndex{ f, idx->

            String nombre = f[cite.cameres][cite.carretera].text()+" "+f[cite.cameres][cite.municipi].text()

            if( f[cite.cameres][cite.pk].text() )
                nombre +=" PK "+f[cite.cameres][cite.pk].text()

            Camara c = new Camara(ciudad: 'barcelona',
                    id: idx,
                    nombre: nombre,
                    url: f[cite.cameres][cite.link].text()
            )
            camaras.add c
        }
    }

}
