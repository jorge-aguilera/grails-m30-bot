package grails.m30.bot.services

import groovy.util.logging.Log

@Log
class BarilocheService extends ListUrl2GifService{

    @Override
    String getServiceName() {
        "bariloche"
    }

    @Override
    String[] getListUrls() {
        appConfig.barilocheCamaras
    }

}
