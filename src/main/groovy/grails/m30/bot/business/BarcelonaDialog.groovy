package grails.m30.bot.business


import grails.m30.bot.services.BarcelonaService

class BarcelonaDialog implements SearchDialogTrait{

    BarcelonaService searchCamaraService

    void executeCommand() {
        switch(command.toLowerCase()){
            case '/dalt':
                command="29,31,32,41"
                buildSearch('Ronda de Dalt')
            break
            case '/litoral':
                command="30,39,40,47"
                buildSearch('Ronda Litoral')
                break
            default:
                buildSorry()
                break
        }
    }

    String welcomeMessage = """
Hola. Soy un simple bot que intenta mostrarte el estado de las cámaras de tráfico de Barcelona
Por ahora no hago mucho porque estoy en pruebas. Mientras prueba estos comandos

/start  (este mensaje)
/camaras  (para ver un listado de todas las camaras)
/dalt    Ronda de Dalt
/litoral Ronda Litoral 

Cualquier otro texto intentaré buscar una calle que se parezca y enviarte su estado

Made with ❤ by PuraVida Software
"""
}
