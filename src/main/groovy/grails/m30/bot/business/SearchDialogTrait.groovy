package grails.m30.bot.business

import com.puravida.gif.GifGenerator
import grails.async.Promises
import grails.m30.bot.services.BarilocheService
import grails.m30.bot.SearchCamaraResult
import grails.m30.bot.services.SearchCamaraService
import grails.m30.bot.telegram.Message
import grails.m30.bot.telegram.TelegramApi
import groovy.transform.CompileStatic
import groovy.transform.TypeCheckingMode

trait SearchDialogTrait {

    TelegramApi telegramApi
    String chatId
    String command

    BarilocheService barilocheService

    abstract SearchCamaraService getSearchCamaraService()

    abstract String getWelcomeMessage()

    abstract void executeCommand()

    void execute(){
        switch( command.toLowerCase() ){
            case '/start':
                buildStart()
                break
            case '/camaras':
                buildCamaras()
                break
            case '/bariloche':
                buildBariloche()
                break
            default:
                if( command.startsWith('/'))
                    executeCommand()
                else
                    buildSearch()
                break
        }
    }



    void buildStart(){
        Message msg = new Message(chat_id: chatId, text: welcomeMessage)
        telegramApi.sendMessage(msg)
    }

    void buildCamaras(){
        List<String> ret = ["*Ubicaciones de las cámaras*"]
        searchCamaraService.camaras().collate(100).eachWithIndex { group, idx ->

            if (idx) ret.clear()

            group.each {
                ret.add it.toString()
            }

            String txt = String.join('\n', ret as String[])
            Message msg = new Message(chat_id: chatId, text: txt)
            telegramApi.sendMessage(msg)
        }

        Message msg = new Message(chat_id: chatId, text: "Recuerda que puedes pedirme una camara por su nombre o el id")
        telegramApi.sendMessage(msg)
    }

    void buildSorry(){
        String txt = "Upsss no hay nada con esa busqueda\nRecuerda que con /camaras puedes ver la lista completa de ubicaciones"
        Message msg = new Message(chat_id: chatId, text: txt)
        telegramApi.sendMessage(msg)
        return
    }

    @CompileStatic(TypeCheckingMode.SKIP)
    void buildSearch(String caption=null){

        List<SearchCamaraResult> result = []
        command.split(',').each { search ->
            SearchCamaraResult searchCamaraResult = searchCamaraService.searchCamara(search)
            if (searchCamaraResult.name)
                result.add searchCamaraResult
        }

        if( !result ){
            buildSorry()
            return
        }

        if( result.size() == 1){
            SearchCamaraResult searchCamaraResult = result.first()
            telegramApi.sendPhoto(chatId, searchCamaraResult.image, searchCamaraResult.name)
            if (searchCamaraResult.alternates) {
                List<String> txt = ["Calles similares:"]
                txt.addAll searchCamaraResult.alternates.take(3)*.toString()
                Message msg = new Message(chat_id: chatId, text: String.join('\n', txt))
                telegramApi.sendMessage(msg)
            }
            return
        }

        List<byte[]>files = result.collect{it.image}
        GifGenerator gifGenerator = new GifGenerator()
        byte[] gif = gifGenerator.composeBytes(files, 2000, true)
        telegramApi.sendAnimation(chatId, gif, caption?:command)
    }

    void buildBariloche(){
        Message msg = new Message(chat_id: chatId, text: "Preparando tu imagen con las últimas camaras disponibles")
        telegramApi.sendMessage(msg)
        Promises.task {
            try {
                telegramApi.sendAnimation(chatId, barilocheService.getGif(), "Bariloche")
            } catch (e) {
                telegramApi.sendMessage(new Message(
                        chat_id: chatId,
                        text: "Ummmm, no tengo la imagen preparada toavía. Prueba más tarde porfa"))
            }
        }
    }
}