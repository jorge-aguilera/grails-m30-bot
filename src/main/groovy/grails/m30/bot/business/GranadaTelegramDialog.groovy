package grails.m30.bot.business

import grails.events.annotation.Subscriber
import grails.m30.bot.AppConfig
import grails.m30.bot.events.UserMessageEvent
import grails.m30.bot.services.BarilocheService
import grails.m30.bot.services.GranadaService
import grails.m30.bot.services.SierraNevadaService
import grails.m30.bot.telegram.TelegramApi
import grails.m30.bot.telegram.TelegramApiFactory
import groovy.util.logging.Log
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

import javax.annotation.PostConstruct

@Log
@Component
class GranadaTelegramDialog {

    @Autowired
    AppConfig appConfig

    @Subscriber('userMessageEvent')
    void onUserMessageEvent(UserMessageEvent  userMessageEvent) {

        if (userMessageEvent.city != 'Granada')
            return

        TelegramApi telegramApi = TelegramApiFactory.newTelegramApi(appConfig, 'granada')

        GranadaService granadaService = new GranadaService(appConfig:appConfig)

        BarilocheService barilocheService = new BarilocheService(appConfig: appConfig)

        SierraNevadaService sierraNevadaService = new SierraNevadaService(appConfig: appConfig)

        new GranadaDialog(
                sierraNevadaService:sierraNevadaService,
                barilocheService: barilocheService,
                searchCamaraService: granadaService,
                chatId: userMessageEvent.chat_id,
                command: userMessageEvent.text,
                telegramApi: telegramApi,
        ).execute()

    }

}
