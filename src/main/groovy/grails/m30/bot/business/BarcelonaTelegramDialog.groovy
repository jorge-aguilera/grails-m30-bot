package grails.m30.bot.business

import grails.events.annotation.Subscriber
import grails.m30.bot.AppConfig
import grails.m30.bot.events.UserMessageEvent
import grails.m30.bot.services.BarcelonaService
import grails.m30.bot.services.BarilocheService
import grails.m30.bot.telegram.TelegramApi
import grails.m30.bot.telegram.TelegramApiFactory
import groovy.util.logging.Log
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

import javax.annotation.PostConstruct

@Log
@Component
class BarcelonaTelegramDialog {

    @Autowired
    AppConfig appConfig

    @Subscriber('userMessageEvent')
    void onUserMessageEvent(UserMessageEvent  userMessageEvent) {

        if (userMessageEvent.city != 'Barcelona')
            return

        TelegramApi telegramApi = TelegramApiFactory.newTelegramApi(appConfig,'barcelona')

        BarcelonaService barcelonaService = new BarcelonaService(appConfig: appConfig)

        BarilocheService barilocheService = new BarilocheService(appConfig: appConfig)

        new BarcelonaDialog(
                barilocheService: barilocheService,
                searchCamaraService: barcelonaService,
                chatId: userMessageEvent.chat_id,
                command: userMessageEvent.text,
                telegramApi: telegramApi,
        ).execute()

    }

}
