package grails.m30.bot.business

import grails.events.annotation.Subscriber
import grails.m30.bot.AppConfig
import grails.m30.bot.events.UserMessageEvent
import grails.m30.bot.services.BarilocheService
import grails.m30.bot.services.DgtService
import grails.m30.bot.services.M30Service
import grails.m30.bot.services.MadridService
import grails.m30.bot.telegram.TelegramApi
import grails.m30.bot.telegram.TelegramApiFactory
import groovy.util.logging.Log
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

import javax.annotation.PostConstruct

@Log
@Component
class MadridTelegramDialog {

    @Autowired
    AppConfig appConfig

    @Subscriber('userMessageEvent')
    void onUserMessageEvent(UserMessageEvent  userMessageEvent){

        if( userMessageEvent.city != 'Madrid')
            return

        TelegramApi telegramApi = TelegramApiFactory.newTelegramApi(appConfig, 'madrid')

        M30Service m30Service = new M30Service(appConfig)

        MadridService madridService = new MadridService(appConfig: appConfig)

        BarilocheService barilocheService = new BarilocheService(appConfig: appConfig)

        DgtService dgtService = new DgtService(appConfig: appConfig)

        new MadridDialog(
                chatId: userMessageEvent.chat_id,
                command: userMessageEvent.text,
                barilocheService: barilocheService,
                searchCamaraService: madridService,
                m30Service: m30Service,
                dgtService: dgtService,
                telegramApi: telegramApi,
        ).execute()

    }
}
