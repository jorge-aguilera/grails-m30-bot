package grails.m30.bot.business

import grails.async.Promises
import grails.m30.bot.services.GranadaService
import grails.m30.bot.services.SierraNevadaService
import grails.m30.bot.telegram.Message


class GranadaDialog implements SearchDialogTrait{

    GranadaService searchCamaraService

    SierraNevadaService sierraNevadaService

/*
Gracias a @auditae por las agrupaciones:
Ruta 1: Circunvalación.
Id cámaras: 27,28,43

Ruta 2: Acceso sur desde A-44
Id cámaras: 43,28,24,26,25,21,16,11,12

Ruta 3: Acceso desde norte a centro
Id cámaras: 1,4,5,19,18,29,30,40

Ruta 4: Acceso desde A44 a La Caleta
Id cámaras: 43,35,30,29,18,19,7,6

Ruta 5: Calle Palencia - Severo Ochoa
Id cámaras: 51,49,36,31,24,25,21,16

Ruta 6: Fernando de los Ríos a Centro
Id cámaras: 55,54,49,39,40,30

camaras - Listado de todas las camaras
circunvalacion - Circunvalacion
salida - Acceso sur desde A-44
nortecentro - Acceso desde norte a centro
surcaleta - Acceso desde A44 a La Caleta
surochoa - Calle Palencia - Severo Ochoa
surcentro - Fernando de los Ríos a Centro
*/

    void executeCommand() {
        switch(command.toLowerCase()){
            case '/circunvalacion':
                command="27,28,43"
                buildSearch('Circunvalacion')
                break
            case '/salida':
                command="43,28,24,26,25,21,16,11,12"
                buildSearch('A-44')
                break
            case '/nortecentro':
                command="1,4,5,19,18,29,30,40"
                buildSearch('Norte-Centro')
                break
            case '/surcaleta':
                command="43,35,30,29,18,19,7,6"
                buildSearch('La Caleta')
                break
            case '/surochoa':
                command="51,49,36,31,24,25,21,16"
                buildSearch('Palencia-Ochoa')
                break
            case '/surcentro':
                command="55,54,49,39,40,30"
                buildSearch('FernandoRios-Centro')
                break
            case '/sierra':
                buildSierraNevada()
                break
            default:
                buildSorry()
                break
        }
    }

    String welcomeMessage = """
Hola. Soy un simple bot que intenta mostrarte el estado de las cámaras de tráfico de Granada
Por ahora no hago mucho porque estoy en pruebas. Mientras prueba estos comandos

/start  (este mensaje)
/camaras  (para ver un listado de todas las camaras)
/circunvalacion - Circunvalacion
/salida - Acceso sur desde A-44
/nortecentro - Acceso desde norte a centro
/surcaleta - Acceso desde A44 a La Caleta
/surochoa - Calle Palencia - Severo Ochoa
/surcentro - Fernando de los Ríos a Centro

Cualquier otro texto intentaré buscar una calle que se parezca y enviarte su estado

Made with ❤ by PuraVida Software
"""

    void buildSierraNevada(){
        Message msg = new Message(chat_id: chatId, text: "Preparando tu imagen con las últimas camaras disponibles")
        telegramApi.sendMessage(msg)
        Promises.task {
            try {
                telegramApi.sendAnimation(chatId, sierraNevadaService.gif, 'SierraNevada')
            } catch (e) {
                telegramApi.sendMessage(new Message(
                        chat_id: chatId,
                        text: "Ummmm, no tengo la imagen preparada toavía. Prueba más tarde porfa"))
            }
        }
    }

}
