package grails.m30.bot.business


import grails.async.Promises
import grails.m30.bot.services.DgtService
import grails.m30.bot.services.M30Service
import grails.m30.bot.services.MadridService
import grails.m30.bot.telegram.Message
import groovy.util.logging.Log

@Log
class MadridDialog implements SearchDialogTrait{

    MadridService searchCamaraService

    M30Service m30Service
    DgtService dgtService

    String welcomeMessage = """
Hola. Soy un simple bot que intenta mostrarte el estado de las cámaras de tráfico de Madrid
Estos son los comandos que tengo actualmente implementados:

- /m30  (te enviaré un gif animado con las fotos de la m30)
- /a6  (te enviaré un gif animado con las fotos de la A6-Madrid)
- /status (te enviaré un resumen del estado: velocidad media, vehiculos circulando...)
- /camaras (te muestro un listado con todas las calles donde hay cámaras)

Cualquier otro texto intentaré buscar una calle que se parezca y enviarte su estado

Made with ❤ by PuraVida Software
"""

    void executeCommand(){
        switch( command.toLowerCase() ){
            case '/status':
                buildM30Status()
                break
            case '/m30':
                buildM30()
                break
            case '/a6':
                buildDGT('a6','A6')
                break
            case '/m40norte':
                buildDGT('m40norte','M40-Norte')
                break
            case '/m40sur':
                buildDGT('m40sur','M40-Sur')
                break
            case '/m40oeste':
                buildDGT('m40oeste','M40-Oeste')
                break
            case '/m40este':
                buildDGT('m40este','M40-Este')
                break
            case '/m111':
                buildDGT('m111','M-111')
                break
            case '/a5entrada':
                buildDGT('a5entrada','A5')
                break
            case '/a5salida':
                buildDGT('a5salida','A5')
                break
            default:
                buildSorry()
                break
        }
    }

    void buildM30Status(){
        Message msg = new Message(chat_id: chatId, text: m30Service.currentStatus())
        log.info "Sending current status to $chatId"
        telegramApi.sendMessage(msg)
    }


    void buildM30(){
        Message msg = new Message(chat_id: chatId, text: "Preparando tu imagen con las últimas camaras disponibles")
        telegramApi.sendMessage(msg)
        Promises.task {
            log.info "Sending current m30 to $chatId"
            try {
                telegramApi.sendAnimation(chatId, m30Service.gif, 'M30')
            } catch (e) {
                log.warning e.toString()
                telegramApi.sendMessage(new Message(
                        chat_id: chatId,
                        text: "Ummmm, no tengo la imagen preparada toavía. Prueba más tarde porfa"))
            }
        }
    }

    void buildDGT(String service, String title){
        Message msg = new Message(chat_id: chatId, text: "Preparando tu imagen con las últimas camaras disponibles")
        telegramApi.sendMessage(msg)
        Promises.task {
            try {
                log.info "Sending current $service to $chatId"
                telegramApi.sendAnimation(chatId, dgtService.getGif(service), title)
            } catch (e) {
                log.warning e.toString()
                telegramApi.sendMessage(new Message(
                        chat_id: chatId,
                        text: "Ummmm, no tengo la imagen preparada toavía. Prueba más tarde porfa"))
            }
        }
    }

}
