package grails.m30.bot

import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component

@Component
class AppConfig {

    @Value('${telegram.tokens.madrid}')
    String madridToken

    @Value('${telegram.tokens.barcelona}')
    String barcelonaToken

    @Value('${telegram.tokens.granada}')
    String granadaToken

    String[] barilocheCamaras = [
            "https://bariloche.org/camaras_web/2/bariloche.jpg",
            "https://bariloche.org/webcam_civico/image.jpg",
            "https://catedralaltapatagonia.com/partediario/images_webcams/foto_cam_01.jpg",
            "https://catedralaltapatagonia.com/partediario/images_webcams/foto_cam_02.jpg",
            "https://catedralaltapatagonia.com/partediario/images_webcams/foto_cam_04.jpg",
            "https://catedralaltapatagonia.com/partediario/images_webcams/foto_cam_05.jpg",
            "https://catedralaltapatagonia.com/partediario/images_webcams/foto_cam_06.jpg",
            "https://catedralaltapatagonia.com/partediario/images_webcams/foto_cam_07.jpg",
            "https://catedralaltapatagonia.com/partediario/images_webcams/foto_cam_08.jpg",
    ]

    String[] sierraNevadaCamaras = [
            "https://sierranevada.es/_extras/fotos_camaras/borreguiles/snap.jpg",
            "https://sierranevada.es/_extras/fotos_camaras/zayas/current.jpg",
            "https://sierranevada.es/_extras/fotos_camaras/lagunainf/snap.jpg",
            "https://sierranevada.es/_extras/fotos_camaras/mobotix/current.jpg",
            "https://sierranevada.es/_extras/fotos_camaras/borreguilestaller/snap.jpg",
            "https://sierranevada.es/_extras/fotos_camaras/tlcabinasup/sierranevada.jpg",
            "https://sierranevada.es/_extras/fotos_camaras/camaramsn/MeliaSyN.jpg",
            "https://sierranevada.es/_extras/fotos_camaras/montebajo/current.jpg",
            "https://sierranevada.es/_extras/fotos_camaras/zayas/pistacordon.jpg",
            "https://www.osn.iaa.csic.es/sites/default/files/camOSN/OSN_Sur.jpg",
            "http://www.osn.iaa.es/sites/default/files/camOSN/OSN_Norte.jpg",
            "http://infocar.dgt.es/etraffic/data/camaras/313.jpg",
    ]

    String barcelonaUrl='http://www.gencat.cat/transit/opendata/cameres.xml'

    String granadaUrl='http://www.movilidadgranada.com/tra/matriz.php'

    String api='https://api.telegram.org/bot'

    String dgtUrl = 'http://infocar.dgt.es/etraffic/data/camaras/'

    String[] m40norte = ["1013","1006","1001","600","996","1003","651","649","995","1009","653","1096","657"]

    String[] m40sur = ["998","930","932","820","829","821","828","827","822","826","823","824"]

    String[] m40este = ["652","655","929","922","925","928","927","920","926","1007"]

    String[] m40oeste = ["1008","1000","993","1010","997","1004","994"]

    String[] a6 = ["866","862","878","868","1161","893","879","884","900","894","901"]

    String[] m111 = ["1091","1092"]

    String[] a5salida = ["1125","798","797","788","790","795","791","792","796","789","799","842","843","837","841","844","838","839","845"]

    String[] a5entrada = a5salida.reverse()

    String m30Url='http://www.mc30.es/components/com_hotspots/datos/camaras.xml'

}

