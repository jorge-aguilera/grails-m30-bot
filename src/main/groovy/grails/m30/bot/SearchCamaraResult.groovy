package grails.m30.bot

class SearchCamaraResult {
    String search
    String name
    byte[] image
    List<Camara> alternates
}
