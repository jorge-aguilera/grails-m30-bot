package grails.m30.bot.telegram

import groovy.transform.ToString

@ToString
class UserMessage {

    int message_id

    From from

    Chat chat

    long date

    String text

}
