package grails.m30.bot.telegram

import groovy.transform.ToString

@ToString
class Chat {
    String id
    String first_name
    String last_name
    String username
    String type
}
