package grails.m30.bot.telegram

import groovy.transform.ToString

@ToString
class Result {
    int message_id
    From from
    Chat chat
    Date date
    String text
}
