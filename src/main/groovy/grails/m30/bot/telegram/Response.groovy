package grails.m30.bot.telegram

import groovy.transform.ToString

@ToString
class Response<T> {

    boolean ok;

    T result;

}
