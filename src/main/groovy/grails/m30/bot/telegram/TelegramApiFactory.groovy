package grails.m30.bot.telegram

import grails.m30.bot.AppConfig
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component

import javax.annotation.PostConstruct

class TelegramApiFactory {

    static TelegramApi newTelegramApi(AppConfig appConfig, String id){
        String token = appConfig.getProperty("${id}Token") as String
        assert  token

        new TelegramApi(appConfig.api, token)
    }


}
