package grails.m30.bot.telegram

import groovy.util.logging.Log
import groovyx.net.http.HttpBuilder
import groovyx.net.http.NativeHandlers
import groovyx.net.http.OkHttpEncoders

import static groovyx.net.http.ContentTypes.BINARY
import static groovyx.net.http.ContentTypes.JSON
import static groovyx.net.http.MultipartContent.multipart

import static groovyx.net.http.HttpBuilder.configure

@Log
class TelegramApi {

    String api

    String token

    HttpBuilder restBuilder

    TelegramApi(String api, String token){
        this.api = api
        this.token = token
        restBuilder = configure {
            //request.uri = "$api$token"
            request.contentType = JSON[0]
        }
    }

    void sendMessage( Message message){
        log.info "sendMessage to $message.chat_id $api$token"
        restBuilder.post{
            request.raw = "$api$token/sendMessage"
            request.body = [
                chat_id : message.chat_id,
                text : message.text,
                parse_mode : message.parse_mode
            ]
        }
    }

    void sendPhoto(String chat_id, byte[]bytes, String caption=null) {
        File file = File.createTempFile("telegram", ".jpg")
        file.bytes = bytes
        sendPhoto(chat_id,file,caption)
        file.delete()
    }

    void sendPhoto(String chat_id, File file, String caption=null){
        restBuilder.post{
            request.raw = "$api$token/sendPhoto"
            request.contentType = "multipart/form-data"
            request.body= multipart {
                field 'chat_id' , chat_id
                field 'caption',  caption ?: ""
                part 'photo' , file.name, "application/octet-stream", file
            }
            request.encoder BINARY, NativeHandlers.Encoders.&binary
            request.encoder 'multipart/form-data', OkHttpEncoders.&multipart
        }
    }

    void sendAnimation(String chat_id, byte[]bytes, String caption=null) {
        File file = File.createTempFile("telegram", ".gif")
        file.bytes = bytes
        sendAnimation(chat_id,file,caption)
        file.delete()
    }

    void sendAnimation(String chat_id, File file, String caption=null) {
        restBuilder.post{
            request.raw = "$api$token/sendAnimation"
            request.contentType = "multipart/form-data"
            request.body= multipart {
                field 'chat_id', chat_id
                field 'caption', caption ?: ""
                part 'animation' , file.name, "application/octet-stream", file
            }
            request.encoder BINARY, NativeHandlers.Encoders.&binary
            request.encoder 'multipart/form-data', OkHttpEncoders.&multipart
        }
    }

}
