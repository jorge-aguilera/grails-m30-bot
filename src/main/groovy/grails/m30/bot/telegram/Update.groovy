package grails.m30.bot.telegram

import grails.validation.Validateable
import groovy.transform.ToString

@ToString
class Update implements Validateable{

    int update_id;

    UserMessage message;

}
