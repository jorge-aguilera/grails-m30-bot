package grails.m30.bot.events


import groovy.transform.ToString

@ToString
class UserMessageEvent{

    String city

    String chat_id
    String username
    String text

}
