package grails.m30.bot

import grails.core.GrailsApplication
import grails.plugins.*
import groovy.util.logging.Log

@Log
class ApplicationController {

    def index() {
        render status:200
    }

    def start(){
        log.info "star"
        render status:200
    }

    def stop(){
        log.info "stop"
        render status:200
    }

    def error(){
        log.info "error"
        render status:200
    }
}
