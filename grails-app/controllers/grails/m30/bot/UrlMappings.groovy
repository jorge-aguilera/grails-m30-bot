package grails.m30.bot

import groovy.transform.CompileStatic
import groovy.transform.TypeCheckingMode

@CompileStatic(TypeCheckingMode.SKIP)
class UrlMappings {

    static mappings = {
        delete "/$controller/$id(.$format)?"(action:"delete")
        get "/$controller(.$format)?"(action:"index")
        get "/$controller/$id(.$format)?"(action:"show")
        post "/$controller(.$format)?"(action:"save")
        put "/$controller/$id(.$format)?"(action:"update")
        patch "/$controller/$id(.$format)?"(action:"patch")

        post "/esteeselpuntodeentradaalbot/$action"(controller: 'bot')

        "/"(controller: 'application', action:'index')
        "500"(controller: 'application', action:'error')
        "404"(controller: 'application', action:'error')

        "/_ah/start"(controller: 'application', action:'start')
        "/_ah/stop"(controller: 'application', action:'stop')

        get "/esteeselpuntodeentradaalbot/testMadrid"(controller: 'bot', action: 'testMadrid')
    }
}
