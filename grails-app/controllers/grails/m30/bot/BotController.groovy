package grails.m30.bot

import grails.events.EventPublisher

import grails.m30.bot.events.UserMessageEvent
import grails.m30.bot.telegram.Chat
import grails.m30.bot.telegram.From
import grails.m30.bot.telegram.Message
import grails.m30.bot.telegram.Update
import grails.m30.bot.telegram.UserMessage
import groovy.util.logging.Log

@Log
class BotController implements EventPublisher {

    def madrid(Update update){
        notifyEvent('Madrid', update)
        render status:200
    }

    def barcelona(Update update){
        notifyEvent('Barcelona', update)
        render status:200
    }

    def granada(Update update){
        notifyEvent('Granada', update)
        render status:200
    }

    void notifyEvent(String city, Update update){
        log.info update.toString()
        UserMessageEvent userMessageEvent = new UserMessageEvent(
                city: city,
                chat_id: update.message.chat.id,
                username: update.message.chat.username,
                text: update.message.text,
        )

        notify( 'userMessageEvent', userMessageEvent)
        log.info "notyfied $update"
    }

}
